import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex)

const colorAPI ='http://127.0.0.1:8000'

export default new Vuex.Store({
  state: {
      colors:[],
      schemes:[]

  },
  mutations: {
    ADD_COLOR(state, color){
        state.colors.push(color);
    },

    REMOVE_COLOR(state, color) {
      state.colors = state.colors.filter(item => item.id !== color.id);
    },


    FETCH_COLOR(state, color){
        state.colors=color;
    },
  },
  actions: {


   async addColors({commit}, color) {
    let response = await Axios.post(colorAPI+'/color', color);
  commit('ADD_COLOR', response.data);
},
async removeColor({commit}, color) {
      await Axios.delete(colorAPI+'/color/remove/'+ color.id);
commit('REMOVE_COLOR',color);
},
  async fetchColors({commit}) {
    let response = await Axios.get(colorAPI+'/color');
    commit('FETCH_COLOR', response.data);
  },
    }

})
